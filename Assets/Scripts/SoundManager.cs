﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Class made with the help of great tutorials from Code Monkeys !

public static class SoundManager
{
    //maxi sound list
    public enum Sound
    {
        back,
        select,
        bonus, 
        malus, 
        lap,
        hover,
        finish, 
        track1,
        track2,
        track3,
        explosion,
        ship,
        bip,
        go
    }

    private static Dictionary<Sound, float> soundTimerDic;

    public static void Initialize()
    {
        soundTimerDic = new Dictionary<Sound, float>();
        soundTimerDic[Sound.hover] = 0f;
    }

    public static void PlaySound(Sound sound)
    {
        if (!CanPlaySound(sound))
            return;

        GameObject soundGO = new GameObject("Sound");
        AudioSource audioSource = soundGO.AddComponent<AudioSource>();
        audioSource.PlayOneShot(GetAudioClip(sound));
    }

    private static void PlaySound(Vector3 position, Sound sound)
    {
        if (!CanPlaySound(sound))
            return;

        GameObject soundGO = new GameObject("Sound");
        soundGO.transform.position = position;

        AudioSource audioSource = soundGO.AddComponent<AudioSource>();
        audioSource.clip = GetAudioClip(sound);
        audioSource.Play();
    }

    private static bool CanPlaySound(Sound sound)
    {
        switch (sound)
        {
            case Sound.hover:

                if (soundTimerDic.ContainsKey(sound))
                {
                    float lastTimePlayed = soundTimerDic[sound];
                    float hoverTimerMax = .15f;

                    if(lastTimePlayed + hoverTimerMax < Time.time)
                    {
                        soundTimerDic[sound] = Time.time;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }

            default:
                return true;
        }
    }

    private static AudioClip GetAudioClip(Sound sound)
    {
        foreach(GameAssets.SoundAudioClip soundAudioClip in GameAssets.Instance.soundAudioClipArray)
        {
            if(soundAudioClip.sound == sound)
            {
                return soundAudioClip.audioClip;
            }
        }

        Debug.LogError("Sound " + sound + "not found");
        return null;
    }

    public static void AddButtonSounds(this Button button)
    {
        button.onClick.AddListener(delegate { SoundManager.PlaySound(Sound.select); });
    }
}
