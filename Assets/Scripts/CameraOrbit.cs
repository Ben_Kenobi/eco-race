﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOrbit : MonoBehaviour
{
    [SerializeField]
    private float cameraOrbitSpeed = 1.2f;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, cameraOrbitSpeed * Time.deltaTime, 0);
    }
}
