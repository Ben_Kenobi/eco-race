﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceTimers : MonoBehaviour
{   
    //Timer elements
    public int minCount = 0;
    public int secCount = 0;
    public float milliCount = 0f;
    public string milliDisplay = "";

    //Labels
    public TMPro.TextMeshProUGUI minLabel;
    public TMPro.TextMeshProUGUI secLabel;
    public TMPro.TextMeshProUGUI milliLabel;

    //---------LAP label-------------
    public TMPro.TextMeshProUGUI bestlapMinLabel;
    public TMPro.TextMeshProUGUI bestlapSecLabel;
    public TMPro.TextMeshProUGUI bestlapMilliLabel;

    //Best lap
    private int bestLapMin = 0;
    private int bestLapSec = 0;
    private float bestLapMilli = 0f;

    //last lap time
    private int lastLapMin = 0;
    private int lastLapSec = 0;
    private float lastLapMilli = 0f;

    //current lap tim
    private int currentLapMin = 0;
    private int currentLapSec = 0;
    private float currentLapMilli = 0f;

    private bool hasBestLap = false;
    public bool timerPaused;

    public Starter starterRef;

    void Start()
    {
        if(minLabel == null || secLabel == null || milliLabel == null ||
           bestlapMinLabel == null || bestlapSecLabel == null || bestlapMilliLabel == null)
        {
            Debug.LogWarning("not all label setted for timers");
            return;
        }

        if(starterRef == null)
        {
            Debug.LogError("no starter ref setted");
            return;
        }
    }

    void Update()
    {
        if (!starterRef.raceStart || starterRef.raceEnded || timerPaused)
            return;

        milliCount += Time.deltaTime * 10;
        currentLapMilli += Time.deltaTime * 10;

        milliDisplay = milliCount.ToString("F0");

        if(milliCount >= 10)
        {
            milliCount = 0;
            currentLapMilli = 0;

            secCount += 1;
            currentLapSec += 1;
        }

        if(secCount >= 60)
        {
            secCount = 0;
            currentLapSec = 0;

            minCount += 1;
            currentLapMin += 1;
        }

        UpdateTimerText();
    }

    private void SetBestLap()
    {
        hasBestLap = true;

        bestlapMinLabel.SetText(FormatTime(bestLapMin));
        bestlapSecLabel.SetText(FormatTime(bestLapSec));
        bestlapMilliLabel.SetText(bestLapMilli.ToString("F0"));
    }

    private void CompareTimer()
    {
        if (!hasBestLap)
        {
            bestLapMin = minCount;
            bestLapSec = secCount;
            bestLapMilli = milliCount;

            SetBestLap();
        }
        else
        {
            float totalCurrentLap = currentLapMin * 60 * 1000 + lastLapSec * 1000 + lastLapMilli;
            float totalBestLap = bestLapMin * 60 * 1000 + bestLapSec * 1000 + bestLapMilli;

            if (totalCurrentLap < totalBestLap)
                SetBestLap();
        }
    }

    private void UpdateTimerText()
    {
        milliLabel.SetText(milliDisplay);
        secLabel.SetText(FormatTime(secCount));
        minLabel.SetText(FormatTime(minCount));
    }

    private void ResetCurrentLap()
    {
        currentLapMin = 0;
        currentLapSec = 0;
        currentLapMilli = 0f;

        lastLapMin = currentLapMin;
        lastLapSec = currentLapSec;
        lastLapMilli = currentLapMilli;
    }

    private string FormatTime(int value)
    {
        if (value < 10)
            return ("0" + value.ToString("F0"));

        return value.ToString("F0");
    }

    public void SetLapTime()
    {
        CompareTimer();
        ResetCurrentLap();
    }

    public string GetBestLap()
    {
        return FormatTime(bestLapMin) + " : " + FormatTime(bestLapSec) + " : " + bestLapMilli.ToString("F0");
    }
}
