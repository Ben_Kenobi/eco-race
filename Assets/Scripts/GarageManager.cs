﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GarageManager : MonoBehaviour
{
    public List<GameObject> ships = new List<GameObject>();
    public GameObject shipOrigin;

    [SerializeField]
    private int index = 0;

    [SerializeField]
    GameObject currentShip;

    // Start is called before the first frame update
    void Start()
    {
        SoundManager.PlaySound(SoundManager.Sound.track2);

        if(ships.Count == 0)
        {
            Debug.LogWarning("no ships");
            return;
        }

        if(shipOrigin == null)
        {
            Debug.LogWarning("no origin for ships");
            return;
        }

        currentShip = ships[index];
        LoadShip(index);
    }

    public void Next()
    {
        if (ships.Count == 0)
            return;

        if (index+1 <= ships.Count-1)
        {
            index++;
        }
        else
        {
            index = 0;
        }

        LoadShip(index);
    }

    public void Previous()
    {
        if (ships.Count == 0)
            return;

        if(index-1 >= 0)
        {
            index--;
        }
        else
        {
            index = ships.Count-1;
        }

        LoadShip(index);
    }

    public void GoToMainMenu()
    {
        Debug.Log("go to main menu");
    }

    private void LoadShip(int index)
    {
        //clear current mesh
        foreach (Transform child in shipOrigin.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        currentShip = ships[index];

        GameObject ship = Instantiate(currentShip, shipOrigin.transform);
        ship.transform.position = shipOrigin.transform.position;
        ship.transform.localScale = new Vector3(1f, 1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
