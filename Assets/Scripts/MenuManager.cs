﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{

    [SerializeField]
    private GameObject[] btns;
    public GameObject ship;
    private bool rotateShip = false;
    public GameObject optionsGroup;

    private void Awake()
    {
        SoundManager.Initialize();
    }

    // Start is called before the first frame update
    void Start()
    {
        SoundManager.PlaySound(SoundManager.Sound.track1);

        btns = GameObject.FindGameObjectsWithTag("Button");

        foreach (GameObject btn in btns)
        {
            Button currentBtn = btn.GetComponent<Button>();
            currentBtn.AddButtonSounds();
        }

        if(optionsGroup == null)
        {
            Debug.LogError("no options group setted");
            return;
        }

        optionsGroup.transform.Find("OptionsBg").GetComponent<Image>().enabled = false;
        optionsGroup.GetComponent<CanvasGroup>().DOFade(0f, 0f);

        if(ship != null)
        {
            rotateShip = true;
        }
    }

    public void ExitApp()
    {
        Application.Quit();
    }

    void Update()
    {
        if (rotateShip)
        {
            UpdateShipRot();
        }
    }

    public void ShowOptions()
    {
        optionsGroup.transform.Find("OptionsBg").GetComponent<Image>().enabled = true;
        optionsGroup.GetComponent<CanvasGroup>().DOFade(1f, 0.5f);
    }

    public void HideOptions()
    {
        optionsGroup.GetComponent<CanvasGroup>().DOFade(0f, 0.5f);
        optionsGroup.transform.Find("OptionsBg").GetComponent<Image>().enabled = false;
    }

    private void UpdateShipRot()
    {
        ship.transform.Rotate(new Vector3(0f, 0.5f * Time.deltaTime, 0f));
    }
}
