﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Starter : MonoBehaviour
{
    public TMPro.TextMeshProUGUI counter;

    [SerializeField]
    private int startIndex = 3;
    [SerializeField]
    private int currentIndex = 3;

    [SerializeField]
    private string startLabel = "GO!";

    private float fromScale = 2.0f;
    private float toScale = 0.5f;

    private float fromFade = 1.0f;
    private float toFade = 0f;

    private float instant = 0f;
    private float timerAnimTime = 1.0f;

    public bool raceStart = false;
    public bool raceEnded = false;

    public RaceMenu raceMenuRef;
    public RaceTimers raceTimersRef;

    //End UI elements
    public GameObject endGroup;
    public CanvasGroup endBtns;
    public TMPro.TextMeshProUGUI bestLap;
    private TMPro.TextMeshProUGUI finishLabel;
    private float endFromScale = 0f;
    private float endToScale = 2f;
    private float endFadeAnimTime = 1.0f;
    public CanvasGroup lapInfos;

    //Die UI elements
    public GameObject gameOverUI;
    private CanvasGroup gameOverBtnsGroup;
    private TMPro.TextMeshProUGUI gameOverLabel;
    private TMPro.TextMeshProUGUI reasonLabel;
    private float goFadeAnimTime = 1.5f;

    //camera fade
    public Image cameraFadeUI;

    //StartLight
    public GameObject startLight;

    void Start()
    {
        DOTween.Init();

        if (counter == null)
        {
            Debug.LogWarning("no start text found");
            return;
        }

        if (endGroup == null)
        {
            Debug.LogWarning("end group ui not found");
            return;
        }

        if(gameOverUI == null)
        {
            Debug.LogWarning("game over group ui not found");
            return;
        }

        if(startLight == null)
        {
            Debug.LogWarning("no material for start light found");
            return;
        }

        if(raceMenuRef == null)
        {
            Debug.LogError("no racemenu script ref found");
            return;
        }

        if(raceTimersRef == null)
        {
            Debug.LogError("no race timer ref setted");
            return;
        }

        if(cameraFadeUI == null)
        {
            Debug.LogError("no camera fade ui setted");
            return;
        }


        endGroup.SetActive(false);
        gameOverUI.SetActive(false);
        endBtns.DOFade(0f, instant);

        SetStartLight(new Color(191f, 0f, 0f, 4.416924f));

        counter.SetText(""); //empty counter text
    }

    private void SetStartLight(Color color)
    {
        startLight.GetComponent<Renderer>().material.SetColor("_EmissionColor", color);
    }

    private void ResetStartUI()
    {
        //reset
        counter.SetText("");
        counter.SetText(currentIndex.ToString());
        counter.DOScale(fromScale, instant);
        counter.DOFade(fromFade, instant);
    }

    public IEnumerator StartCountdown()
    {
        currentIndex = startIndex;
        ResetStartUI();
        SoundManager.PlaySound(SoundManager.Sound.bip);

        //start
        counter.DOScale(toScale, timerAnimTime);
        counter.DOFade(toFade, timerAnimTime);        

        while (currentIndex > 0)
        {
            yield return new WaitForSeconds(1.0f);
            currentIndex--;

            //reset
            counter.SetText("");
            counter.DOScale(fromScale, instant);
            counter.DOFade(fromFade, instant);

            if (currentIndex == 0)
            {
                counter.SetText(startLabel);
                raceStart = true;
                counter.DOFade(toFade, timerAnimTime);
                SetStartLight(new Color(4f, 75f, 0f, 3.310f));
                SoundManager.PlaySound(SoundManager.Sound.go);
            }
            else
            {
                counter.SetText(currentIndex.ToString());
                SoundManager.PlaySound(SoundManager.Sound.bip);
            }

            counter.DOScale(toScale, timerAnimTime);
            counter.DOFade(toFade, timerAnimTime);
        }
    }

    public void ShowEndUI()
    {
        raceMenuRef.ShowOverlay();

        raceEnded = true;
        endGroup.SetActive(true);
        finishLabel = endGroup.transform.Find("Finish").GetComponent<TMPro.TextMeshProUGUI>();

        string time = raceTimersRef.GetBestLap();
        bestLap.SetText(time);

        //set it small first and instantly
        finishLabel.DOScale(endFromScale, instant);
        finishLabel.DOScale(endToScale, endFadeAnimTime);
        lapInfos.DOFade(0f, instant);
        lapInfos.DOFade(1f, 1f);

        StartCoroutine(ShowEndBtns());
    }

    public void DieUI(string msg)
    {
        raceMenuRef.ShowOverlay();

        raceEnded = true;
        gameOverUI.SetActive(true);
        gameOverLabel = gameOverUI.transform.Find("GameOver Label").GetComponent<TMPro.TextMeshProUGUI>();
        reasonLabel = gameOverUI.transform.Find("Message").GetComponent<TMPro.TextMeshProUGUI>();
        gameOverBtnsGroup = gameOverUI.transform.Find("btns").GetComponent<CanvasGroup>();

        gameOverLabel.DOFade(0f, instant);
        gameOverLabel.DOFade(1f, goFadeAnimTime);

        reasonLabel.SetText(msg);
        reasonLabel.DOFade(0f, instant);
        reasonLabel.DOFade(1f, goFadeAnimTime);

        gameOverBtnsGroup.DOFade(0f, instant);
        gameOverBtnsGroup.DOFade(1f, goFadeAnimTime);
    }

    private IEnumerator ShowEndBtns()
    {
        yield return new WaitForSeconds(1.5f);
        endBtns.DOFade(1f, 0.5f);
    }

}
