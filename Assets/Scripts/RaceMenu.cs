﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RaceMenu : MonoBehaviour
{
    //overlay
    public Image overlay;
    private float instant = 0f;

    private bool paused = false;
    public GameObject pauseUi;
    public TMPro.TextMeshProUGUI pauseLabel;
    public CanvasGroup btns;
    private float baseTimeAnim = 0.33f;
    public CanvasGroup resumeBtn;

    public RaceTimers raceTimerRef;
    public ShipController shipCtrlRef;
    public Starter starterRef;

    void Start()
    {
        if (overlay == null)
        {
            Debug.LogWarning("no overlay found");
            return;
        }

        if(pauseUi == null)
        {
            Debug.LogWarning("no pause UI setted");
            return;
        }

        if (raceTimerRef == null)
        {
            Debug.LogError("no race timer ref found");
            return;
        }

        if (shipCtrlRef == null)
        {
            Debug.LogError("no ship ctrl ref found");
            return;
        }

        if(starterRef == null)
        {
            Debug.LogError("no starter ref found");
            return;
        }

        overlay.enabled = false;
        overlay.DOFade(0f, instant);

        pauseUi.SetActive(false);

        SoundManager.PlaySound(SoundManager.Sound.track3);
    }

    void Update()
    {
        if(Input.GetKeyUp(KeyCode.P) || Input.GetKeyUp(KeyCode.Escape))
        {
            if (!starterRef.raceStart)
                return;

            if (paused)
            {
                UnsetPause();
            }
            else
            {
                SetPause();
            }
        }
    }

    public void ShowOverlay()
    {
        overlay.enabled = true;
        overlay.DOFade(0f, instant);
        overlay.DOFade(0.45f, 0.33f);
    }

    public void HideOverlay()
    {
        overlay.enabled = false;
        overlay.DOFade(0f, instant);
    }

    private void ShowPauseUi()
    {
        pauseUi.SetActive(true);
        pauseLabel.DOFade(0f, instant);
        btns.DOFade(0f, instant);
        pauseLabel.DOFade(1f, baseTimeAnim);
        btns.DOFade(1f, baseTimeAnim);
        resumeBtn.DOFade(0f, instant);
        resumeBtn.DOFade(1, baseTimeAnim);
    }

    private void HidePauseUi()
    {
        pauseUi.SetActive(false);
        pauseLabel.DOFade(0f, instant);
        btns.DOFade(0f, instant);
        resumeBtn.DOFade(0f, instant);
    }

    public void UnsetPause()
    {
        paused = false;
        raceTimerRef.timerPaused = false;

        HideOverlay();
        HidePauseUi();

        shipCtrlRef.SetPause();

        return;
    }
    
    public void SetPause()
    {
        paused = true;
        raceTimerRef.timerPaused = true;

        ShowOverlay();
        ShowPauseUi();

        shipCtrlRef.SetPause();
    }
}
