﻿using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CameraManager : MonoBehaviour
{
    public GameObject cameraFade;
    public GameObject shipCamera;
    public GameObject cinematicCamera;
    public Starter starterRef;
    public GameObject trackInfos;

    private Image cameraFadeUI;
    private SplineFollower follower;
    private float slowSpeed = 10f;
    private float slowSpeedRate = 2.5f;
    private float speed = 850f;
    private bool slowingSpeed = false;
    private bool speedIsSlow = false;
    [SerializeField]
    private float defaultAnimTime = 1.25f;
    private float defaultTimerBeforeRaceStart = 0f;
    private bool raceLaunched = false;
    private bool switching = false;

    public GameObject[] playUIs;

    // Start is called before the first frame update
    void Start()
    {
        follower = cinematicCamera.GetComponent<SplineFollower>();
        defaultTimerBeforeRaceStart = defaultAnimTime * 2;

        if (cameraFade == null)
        {
            Debug.LogError("no camera fade ui setted");
            return;
        }

        if(shipCamera == null)
        {
            Debug.LogError("no ship camera found");
            return;
        }

        if(starterRef == null)
        {
            Debug.LogError("starter ref not setted");
            return;
        }

        if(playUIs.Length == 0)
        {
            Debug.LogError("no play uis setted");
            return;
        }

        if(trackInfos == null)
        {
            Debug.LogError("no track infos element setted");
            return;
        }

        HidePlayUIs();

        shipCamera.SetActive(false);
        cameraFadeUI = cameraFade.GetComponent<Image>();
        cameraFadeUI.DOFade(0f, 0f);
        trackInfos.GetComponent<CanvasGroup>().DOFade(0f, 0f);

        ShowTrackInfos();
    }

    // Update is called once per frame
    void Update()
    {
        if (slowingSpeed && !speedIsSlow)
        {
            SlowSpeed();
        }

        if (!switching)
        {
            if (Input.anyKey)
            {
                switching = true;
                StartCoroutine(SkipCameraIntro());
                StartCoroutine(RaceWillLaunch());
            }
        }

        UpdateCameraSpeed();
    }

    public void ChangeCameraSpeed()
    {
        slowingSpeed = true;
    }

    private void SlowSpeed()
    {
        if(speed <= slowSpeed)
        {
            speed = slowSpeed;
            speedIsSlow = true;
        }

        speed -= speed * slowSpeedRate * Time.deltaTime;
    }

    void UpdateCameraSpeed()
    {
        follower.followSpeed = speed;
    }

    private void FadeInCamera()
    {
        cameraFadeUI.DOFade(1f, 1.25f);
    }

    private void FadeOutCamera()
    {
        cameraFadeUI.DOFade(0f, 1.25f);
    }

    public void InitializaRace()
    {
        StartCoroutine(RaceWillLaunch());
    }

    private IEnumerator RaceWillLaunch()
    {
        yield return new WaitForSeconds(defaultTimerBeforeRaceStart);

        if (!raceLaunched)
        {
            raceLaunched = true;            
            StartCoroutine(starterRef.StartCountdown());
            cameraFadeUI.enabled = false;
        }
    }
    
    public void SkipCamera()
    {
        StartCoroutine(SkipCameraIntro());
        switching = true;
    }

    public IEnumerator SkipCameraIntro()
    {
        FadeInCamera();
        HideTrackInfos();
        yield return new WaitForSeconds(1.25f);
        shipCamera.SetActive(true);
        cinematicCamera.SetActive(false);
        ShowPlayUIs();
        FadeOutCamera();
    }

    private void HidePlayUIs()
    {
        foreach(GameObject item in playUIs)
        {
            item.SetActive(false);
        }
    }

    private void ShowPlayUIs()
    {
        foreach(GameObject item in playUIs)
        {
            item.SetActive(true);
        }
    }

    private void ShowTrackInfos()
    {
        trackInfos.GetComponent<CanvasGroup>().DOFade(1f, 1f);
    }

    private void HideTrackInfos()
    {
        trackInfos.GetComponent<CanvasGroup>().DOFade(0f, 1f);
    }
}
