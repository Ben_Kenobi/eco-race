﻿using Dreamteck.Splines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipUI : MonoBehaviour
{
    public RectTransform shieldBar;
    public RectTransform fuelBar;
    public TMPro.TextMeshProUGUI speedMeter;
    public string unit = "Km/h";
    private SplineFollower follower;
    ShipController shipCtrl;

    float maxFuel;
    float maxShield;
    float maxSpeed;

    void Start()
    {
        shipCtrl = GetComponent<ShipController>();
        maxFuel = shipCtrl.maxFuel;
        maxShield = shipCtrl.maxShield;
        maxSpeed = shipCtrl.maxSpeed;
        follower = gameObject.GetComponent<SplineFollower>();

        SetUISpeed(0f);
    }

    public void SetUISpeed(float speed)
    {
        speedMeter.SetText((speed * 4.5f).ToString() + " " + unit); //just for tuning, always better to go faster B)
    }

    public void PickEnergy(float amount)
    {
        float currentEnergy = amount / maxShield;
        shieldBar.localScale = new Vector3(currentEnergy, 1.0f, 1.0f);
    }

    public void UpdateFuel(float amount)
    {
        float current = amount / maxFuel;
        fuelBar.localScale = new Vector3(current, 1.0f, 1.0f);
    }
}
