﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapDetector : MonoBehaviour
{
    public TMPro.TextMeshProUGUI lapTxt;
    public TMPro.TextMeshProUGUI maxLapTxt;

    public Starter starterRef;
    public ShipController shipCtrl;

    [SerializeField]
    private int currentLap = 0;
    private bool firstTimePassed = true;
    public int numberOfLap = 3;

    void Start()
    {
        if (lapTxt == null || maxLapTxt == null)
        {
            Debug.LogWarning("no lap text");
            return;
        }

        if (starterRef == null)
        {
            Debug.LogWarning("No starter reference found");
        }

        if(shipCtrl == null){
            Debug.LogError("no ship controller found");
            return;
        }

        lapTxt.SetText("1");
        maxLapTxt.SetText("/" + numberOfLap.ToString());
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            currentLap++;

            if (currentLap == numberOfLap + 1)
            {
                starterRef.ShowEndUI();
                shipCtrl.RaceEnd();
                return;
            }

            lapTxt.SetText(currentLap.ToString());

            if(currentLap > 0 && firstTimePassed == false)
                gameObject.GetComponentInParent<RaceTimers>().SetLapTime();

            firstTimePassed = false;
        }
    }
}
