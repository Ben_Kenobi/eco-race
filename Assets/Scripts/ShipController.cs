﻿using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{    
    private Rigidbody rb;
    private SplineFollower follower;
    public Starter starterRef;

    [SerializeField]
    private float speed = 0f;
    public float maxSpeed = 250.0f;
    private float acceleratePerSec;
    private float timeZeroToMax = 2.5f;
    private float decelerationRate = 75f;
    private float brakeRate = 5f;

    private float baseYOffset = 0.01f;
    [SerializeField]
    private float maxLeft = -2.0f;
    [SerializeField]
    private float maxRight = 2.0f;
    private float moveSpeed = 7f;

    private bool canAccelerate = true;
    private bool canMove = true;
    private bool dead = false;
    private bool gamePaused = false;

    private float currentShipOffset = 0f;

    //rotation system
    [SerializeField]
    private float currentShipRot = 0f;//initial ship rotation
    private float maxRotAngle = 5f;
    private float minRotAngle = -5f;
    private float rotSpeed = 30f;

    private ShipUI shipUI;

    //Fuel
    public float fuelAmount = 100f;
    private float fuelCons = 0.25f;
    private float maxFuelCons = 3f;
    private float fuelRate = 1.1f;
    public float maxFuel = 100f;
    private float fuelPickup = 50f;
    private bool dieBecauseOfFuel = false;
    [SerializeField]
    private string fuelReason = "No more fuel";

    //Shield
    public float shieldAmout = 100f;
    public float maxShield = 100f;
    public float currentShield = 100f;
    private float shieldPickup = 5f;
    private float shieldDistort = -20f;
    private float distortTime = 2.0f;
    private bool dieBecauseOfShield = false;
    [SerializeField]
    private string shieldReason = "No more shield";

    //paused elements
    private float freezeSpeed;
    private float freezeFuelCons;

    //damaged and other shaders
    public Material damagedMat;
    [SerializeField]
    [ColorUsage(true, true)]
    private Color fuelColor = new Color(0.768f, 0.419f, 1f);
    [SerializeField]
    [ColorUsage(true, true)]
    private Color damagedColor = new Color(1f, 0.808f, 0.031f);
    [SerializeField]
    [ColorUsage(true, true)]
    private Color energyColor = new Color(0.251f, 1f, 0.643f);
    private Material[] matArray;
    private float pickUpShaderAnimTime = 0.5f;

    //explosion effect
    public GameObject explosion;
    //ship child ref
    public GameObject shipMesh;

    //camera movement FX
    public GameObject camera;
    [SerializeField]
    private float cameraPos = 0f;
    private float maxCameraPos = 1f;
    private float baseCameraOffset;
    private float freezeCameraPos;

    //speed effect
    public ParticleSystem speedParticles;
    [SerializeField]
    private float maxParticlesSpeed = 83.22f;
    private float particlesSpeed = 0f;
    [SerializeField]
    private int maxParticlesAmount = 252;
    private int currentParticlesAmount = 0;

    //Exhaust animation
    public Material exhaustMat;
    private float maxExhaust = 2.55f;
    private float currentExhaustAmount = 0f;

    //initialization of the game
    void Awake()
    {
        acceleratePerSec = maxSpeed / timeZeroToMax;
    }

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        follower = gameObject.GetComponent<SplineFollower>();
        follower.followSpeed = 0f;
        shipUI = GetComponent<ShipUI>();

        if(starterRef == null)
        {
            Debug.LogError("no starter ref found");
            return;
        }

        if(damagedMat == null)
        {
            Debug.LogError("no damaged shader set");
            return;
        }

        if(explosion == null)
        {
            Debug.LogError("can't find explosion prefab");
            return;
        }

        if(shipMesh == null)
        {
            Debug.LogError("can't find ship mesh");
            return;
        }

        if(camera == null)
        {
            Debug.LogError("no camera ref set");
            return;
        }

        if(speedParticles == null)
        {
            Debug.LogError("no particle system found");
            return;
        }

        if(exhaustMat == null)
        {
            Debug.LogError("no shader for exhaust found");
            return;
        }

        baseCameraOffset = camera.transform.localPosition.z;
        matArray = shipMesh.GetComponent<MeshRenderer>().materials;

        ResetShipPos();
        MoveShipAlongTrack(currentShipOffset);
        UpdateExhaust();
    }

    void Update()
    {
        if (starterRef.raceStart && !dead && !gamePaused)
        {
            ListenInputs();
        }

        if (dieBecauseOfFuel)
        {
            Decelerate();
            UpdateSpeed();
        }
    }
    
    public void SetPause()
    {
        gamePaused = !gamePaused;

        if (gamePaused)
        {
            freezeSpeed = speed;
            freezeFuelCons = fuelCons;
            freezeCameraPos = cameraPos;

            speed = 0f;
            fuelCons = 0f;
        }
        else
        {
            speed = freezeSpeed;
            fuelCons = freezeFuelCons;
            cameraPos = freezeCameraPos;
        }

        UpdateSpeed();
    }

    private void Die(string why)
    {
        if(why == fuelReason)
        {
            StartCoroutine(NoMoreFuel());
            dieBecauseOfFuel = true;
        }
        else if(why == shieldReason)
        {
            dieBecauseOfShield = true;
            speed = 0f;
        }

        dead = true;
        RaceEnd();
        starterRef.DieUI(why);
    }

    private void Explode()
    {
        UpdateSpeedParticles();
        UpdateExhaust();
        camera.transform.parent = GameObject.Find("Scene").transform;
        SoundManager.PlaySound(SoundManager.Sound.explosion);
        Instantiate(explosion, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    private void Accelerate()
    {
        //accelerate
        speed += acceleratePerSec * Time.deltaTime;
        fuelCons += fuelRate * Time.deltaTime;

        if (fuelCons >= maxFuelCons)
            fuelCons = maxFuelCons;
    }

    private void Brake()
    {
        //brake
        float dec = (decelerationRate * brakeRate);
        speed -= dec * Time.deltaTime;
        fuelCons = 0f;
    }

    private void Decelerate()
    {
        //decelerate
        speed -= decelerationRate * Time.deltaTime;
        fuelCons = 0f;
    }

    private void TurnLeft()
    {
        currentShipOffset -= moveSpeed * Time.deltaTime;
        currentShipRot -= rotSpeed * Time.deltaTime;

        CheckRotOffset();
        CheckShipOffset();
    }

    private void TurnRight()
    {
        currentShipOffset += moveSpeed * Time.deltaTime;
        currentShipRot += rotSpeed * Time.deltaTime;

        CheckRotOffset();
        CheckShipOffset();
    }

    private void CheckShipOffset()
    {
        if (currentShipOffset >= maxRight)
        {
            currentShipOffset = maxRight;
        }
        else if (currentShipOffset <= maxLeft)
        {
            currentShipOffset = maxLeft;
        }
    }

    private IEnumerator NoMoreFuel()
    {
        UpdateSpeedParticles();
        UpdateExhaust();
        yield return new WaitForSeconds(3f);
        Explode();
    }

    private void ListenInputs()
    {
        //acceleration elements
        if (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.UpArrow))
        {
            if (canAccelerate) {
                Accelerate();
            }
            else
            {
                Decelerate();
            }
        }
        else if(Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            Brake();
        }
        else
        {
            Decelerate();
        }

        //move system
        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
        {
            if (!canMove)
                return;

            TurnLeft();
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            if (!canMove)
                return;

            TurnRight();
        }
        else
        {
            AdjustShipRot();
        }

        UpdateSpeed();
        RotateShip();
        MoveShipAlongTrack(currentShipOffset);
        FuelConsumption();
        UpdateCameraPos();
        UpdateSpeedParticles();
        UpdateExhaust();
    }

    private void UpdateCameraPos()
    {   
        cameraPos = maxCameraPos * speed / maxSpeed;
        Vector3 currentCameraPos = camera.transform.localPosition;
        currentCameraPos = new Vector3(currentCameraPos.x, currentCameraPos.y, baseCameraOffset - cameraPos);
        camera.transform.localPosition = currentCameraPos;
    }

    private void CheckRotOffset()
    {
        if (currentShipRot >= maxRotAngle)
        {
            currentShipRot = maxRotAngle;
        }
        else if (currentShipRot <= minRotAngle)
        {
            currentShipRot = minRotAngle;
        }
    }

    private void AdjustShipRot()
    {
        if(currentShipRot > 0.1f)
        {
            currentShipRot -= rotSpeed * Time.deltaTime;
        }
        else if(currentShipRot < -0.1f)
        {
            currentShipRot += rotSpeed * Time.deltaTime;
        }

        if(currentShipRot <= 0.1f && currentShipRot >= -0.1f)
        {
            currentShipRot = 0f;
        }
    }

    private void RotateShip()
    {
        shipMesh.transform.localRotation = Quaternion.AngleAxis(currentShipRot, shipMesh.transform.up);
    }

    private void UpdateSpeed()
    {
        speed = (int)Mathf.Clamp(speed, 0f, maxSpeed);
        follower.followSpeed = speed;
        shipUI.SetUISpeed(speed);
    }

    private void MoveShipAlongTrack(float offset)
    {
        follower.motion.offset = new Vector2(offset, baseYOffset);
    }

    private void ResetShipPos()
    {
        follower.motion.offset = new Vector2(currentShipOffset, 0.19f);
    }

    private void FuelConsumption()
    {
        fuelAmount -= fuelCons * Time.deltaTime;
        if (fuelAmount <= 0)
        {
            fuelAmount = 0f;
            Die(fuelReason);
            return;
        }

        if (fuelAmount >= maxFuel)
            fuelAmount = maxFuel;

        shipUI.UpdateFuel(fuelAmount);
    }

    private void UpdateShield(float amount)
    {
        currentShield += amount;

        if(currentShield <= 0f)
        {
            Die(shieldReason);
            Explode();
            return;
        }

        if (currentShield >= maxShield)
            currentShield = maxShield;

        shipUI.PickEnergy(currentShield);
    }

    private IEnumerator Damaged()
    {
        canAccelerate = false;
        canMove = false;
        yield return new WaitForSeconds(distortTime);
        canAccelerate = true;
        canMove = true;
    }

    private IEnumerator SetShader(Color color, float duration)
    {
        matArray[1] = damagedMat;
        shipMesh.GetComponent<MeshRenderer>().materials = matArray;
        damagedMat.SetColor("Color_4C8FA580", color);

        yield return new WaitForSeconds(duration);

        matArray[1] = null;
        shipMesh.GetComponent<MeshRenderer>().materials = matArray;
    }

    public void RaceEnd()
    {
        canAccelerate = false;
        canMove = false;
    }

    void OnCollisionEnter(Collision col)
    {
        switch (col.gameObject.tag)
        {
            case "Fuel":
                fuelAmount += fuelPickup;
                StartCoroutine(SetShader(fuelColor, pickUpShaderAnimTime));
                SoundManager.PlaySound(SoundManager.Sound.bonus);
                break;
            case "Distortion":
                StartCoroutine(Damaged());
                StartCoroutine(SetShader(damagedColor, distortTime));
                UpdateShield(shieldDistort);
                SoundManager.PlaySound(SoundManager.Sound.malus);
                break;
            case "Energy":
                UpdateShield(shieldPickup);
                StartCoroutine(SetShader(energyColor, pickUpShaderAnimTime));
                SoundManager.PlaySound(SoundManager.Sound.bonus);
                break;

            default:
                Debug.Log("collide with " + col.gameObject.name);
                break;
        }
    }

    private void UpdateSpeedParticles()
    {
        ParticleSystem.MainModule main = speedParticles.main;
        particlesSpeed = maxParticlesSpeed * speed / maxSpeed;

        currentParticlesAmount = maxParticlesAmount * (int)speed / (int)maxSpeed;

        main.startSpeed = particlesSpeed;
        main.maxParticles = currentParticlesAmount;
    }

    private void UpdateExhaust()
    {
        currentExhaustAmount = maxExhaust * speed / maxSpeed;
        exhaustMat.SetFloat("Vector1_49150E55", currentExhaustAmount);
    }
}
